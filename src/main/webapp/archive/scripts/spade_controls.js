/*
 * This file contains the functions common to a number of the
 *  SPADE pages that control features of SPADE.
 */

var ACTIVITY_FLIPSWITCH_PREFIX = 'activity_flipswitch_';

/*
 * Requests a possible change in the state of an application/activity.
 * 
 * param : event The UI event that triggered the invocation.
 */
function requestChange(event) {
	var select = $(this).find('select');
	if (select.flipswitch("option", "disabled")) {
		return;
	}
	event.preventDefault();
	var nameOfChange = select.attr('name');
	if ('application' == nameOfChange) {
		// Note: 'val' has _not_ yet changed in this element!
		updateApp(select.val() == 'on');
	} else if (0 == nameOfChange.indexOf(ACTIVITY_FLIPSWITCH_PREFIX)) {
		var activity = nameOfChange.substring(
				ACTIVITY_FLIPSWITCH_PREFIX.length, nameOfChange.length);
		updateActs([ select.val() == 'on', activity ]);
	}
}

function setFlipswitchTheme(element, existing, replacement) {
	// This is a patch as jquery.mobile does not correctly change the theme.
	var parent = element.closest('.ui-flipswitch')
	parent.removeClass('ui-bar-' + existing).addClass('ui-bar-' + replacement);
	element.flipswitch('option', 'theme', replacement);
}

// Global settings

/*
 * Global variable holding refresh timer for a page.
 */
var pageTimer = null;

/*
 * Global variable holding refresh timer for a chart.
 */
var chartTimer = null;

/*
 * True if this client has access to the 'command' tree of the RESTful
 * interface.
 */
var authorized = false;

/*
 * Check to see if this client can access the 'command' tree of the RESTful
 * interface.
 * 
 * param refresh the function to be called
 * 
 * param : fn the function it invoke to refresh the contents.
 * 
 * param : quantity the quantity with which to invoke the function.
 */
function checkAuthorized(refresh, fn, quantity) {
	var data = null;
	var httpType = "POST";
	var mediaType = 'application/xml';
	var authorizedUrl = '../' + resolveDeployment() + '/command/'
			+ 'authorized';
	$.ajax({
		url : authorizedUrl,
		success : function(data, textStatus, xmlHttpRequest) {
			authorized = true;
			refresh(data);
			refreshContents(fn, quantity, pageTimer);
		},
		error : function(xmlHttpRequest, textStatus, errorThrown) {
			// Assume the error means that the client is not authorized, and do
			// an immediate refresh for the application summary.
			updateApp(null);
		},
		contentType : mediaType,
		data : data,
		type : httpType
	});
}

/*
 * Requests a command be executed by the SPADE application.
 * 
 * param : event The UI event that triggered the invocation.
 */
function executeCommand(event) {
	var command = $(this);
	if ('disabled' == command.attr('disabled')) {
		return;
	}
	command.attr('disabled', 'disabled').button('refresh');
	commandUrl = '../' + resolveDeployment(parseQuery()) + '/command/'
			+ $(this).attr('id');
	$.ajax({
		url : commandUrl,
		success : function(data, textStatus, xmlHttpRequest) {
			command.removeAttr("disabled").button('refresh');
		},
		error : function(xmlHttpRequest, textStatus, errorThrown) {
			if ('Not Found' == errorThrown) {
				alert('Not authorized');
				return;
			}
			if ('' == errorThrown) {
				alert(textStatus);
			} else {
				alert(errorThrown);
			}
		},
		type : "POST"
	});
}
