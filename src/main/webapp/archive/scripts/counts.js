/*
 * Builds arrays that can be used in the chart.
 * 
 * param : data the XML document from which to extract the data.
 */
function buildArrays(data, selection) {
	identities = [];
	threads = [];
	executing = [];
	pending = [];
	total = [];
	var counts;
	if ('application' == selection) {
		counts = $(data).find('counts:not(activity counts)');
	} else {
		counts = $(data).find('activity counts');
	}
	counts.each(function() {
		identities.push($(this).siblings('identity').text());
		threads.push(parseInt($(this).find('threads').text()));
		var executingCount = parseInt($(this).children('executing').text())
		executing.push(executingCount);
		var pendingCount = parseInt($(this).children('pending').text());
		pending.push(pendingCount);
		total.push(executingCount + pendingCount);
	});
	return [ identities, threads, executing, pending, total ];
}

/*
 * Displays updated values in the chart.
 */
function updateDisplayedData(data, destination, selection) {
	var arrays = buildArrays(data, selection);
	chartHeight = 192 + 32 * arrays[0].length;
	$('#' + destination).height(chartHeight);
	var identity = $(data).find('identity:not(activity identity)').text();
	var prefix;
	var suffix;
	if ('application' == selection) {
		prefix = 'Total ';
		suffix = '';
		chart1.xAxis[0].axisTitle.attr({
			text : identity
		});
	} else {
		prefix = '';
		suffix = ' instance ' + identity;
		chart1.series[0].xAxis.setCategories(arrays[0], false);
	}
	chart1.setTitle({
		text : prefix + 'Counts for SPADE' + suffix
	}, {
		text : displayTimestamp(new Date(xmlTimestamp2Date($(data).find(
				"timestamp").text())))
	});
	chart1.series[0].setData(arrays[1], false);
	chart1.series[1].setData(arrays[2], false);
	chart1.series[2].setData(arrays[3], false);
	chart1.series[3].setData(arrays[4], false);
	chart1.setSize(chartWidth, chartHeight);
}

/*
 * Updates the current values in the chart.
 */
function updateChart(args) {
	var destination = args[0];
	var selection = args[1];

	var countUrl = '../' + resolveDeployment(parseQuery())
			+ '/report/application/status';
	$.ajax({
		url : countUrl,
		success : function(data, textStatus, xmlHttpRequest) {
			updateDisplayedData(data, destination, selection);
			refreshContents(updateChart, args, chartTimer);
		},
		error : function(xmlHttpRequest, textStatus, errorThrown) {
			if ('' != errorThrown) {
				alert(errorThrown);
			} else {
				alert(textStatus);
			}
			refreshContents(updateChart, args, chartTimer);
		}
	});
};

/*
 * Creates the chart that will display the current counts.
 */
function createChart(destination, selection) {
	return new Highcharts.Chart({
		chart : {
			renderTo : destination,
			defaultSeriesType : 'bar',
			events : {
				load : updateChart([ destination, selection ])
			}
		},
		credits : {
			enabled : true,
			href : "http://nest.lbl.gov/products/spade/",
			text : 'Spade Data Management'
		},
		title : {
			text : 'Total Counts for SPADE'
		},
		subtitle : {
			text : 'Loading ...'
		},
		xAxis : {
			title : {
				text : 'Activity',
				style : {
					color : Highcharts.theme.colors[0]
				}
			},
			labels : {
				style : {
					minWidth : '140px'
				}
			}
		},
		yAxis : [ {
			title : {
				text : 'counts',
				style : {
					color : Highcharts.theme.colors[0]
				}
			},
			labels : {
				style : {
					color : Highcharts.theme.colors[0]
				}
			}
		} ],
		series : [ {
			name : 'threads'
		}, {
			name : 'executing'
		}, {
			name : 'pending'
		}, {
			name : 'total'
		} ]
	});
}

var chartWidth = $(window).width() - 64;
var chartHeight = 224;

$(window).resize(function() {
	chartWidth = $(window).width() - 64;
	chart1.setSize(chartWidth, chartHeight);
});
