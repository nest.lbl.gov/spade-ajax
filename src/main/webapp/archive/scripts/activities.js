/*
 * This file contains the functions needed on the activities.html page.
 */

// The constant used by the templated entry
var ACTIVITY_TEMPLATE_ITEM = 'activity_template'
var IDENTITY_TEMPLATE_SPAN = 'activity_identity';
var FLIPSWITCH_TEMPLATE_SELECT = 'activity_flipswitch';

var ACTIVITY_ITEM_PREFIX = 'activity_item_';
var ACTIVITY_REQUEST_PREFIX = 'activity_request_';

/*
 * Creates a list element for this specified activity
 * 
 * param : identity the identity of the activity
 */
function createActivityItem(identity, template, list) {
	var element = '<li id="'
			+ ACTIVITY_ITEM_PREFIX
			+ identity
			+ '">\
    <div>\
	    <span style="top: 8px; position: relative;">'
			+ identity
			+ '</span>\
	    <span id="'
			+ ACTIVITY_REQUEST_PREFIX
			+ identity
			+ '" style="float: right;">\
            <select name="'
			+ ACTIVITY_FLIPSWITCH_PREFIX
			+ identity
			+ '" data-mini="true" data-role="flipswitch" data-disabled="true" disabled="disabled">\
                <option value="off">Off</option>\
                <option value="on">On</option>\
            </select>\
        </span>\
    </div>\
</li>'
	list.append(element);
}

/*
 * Updates the list of activities and their current states.
 * 
 * param : data The data from which to extract the current states.
 */
function updateActivityStates(data) {
	var list = $('#activity_states');
	if (0 == list.length) {
		return;
	}
	var activities = $(data).find('activity');
	if (0 == activities.length) {
		var templateSpan = $('#' + IDENTITY_TEMPLATE_SPAN);
		if (0 != templateSpan) {
			templateSpan.text('No Activities');
		}
		return;
	}
	var template = $('#' + ACTIVITY_TEMPLATE_ITEM);
	var loading = 0 != template.length;
	if (loading) {
		activities.each(function() {
			var identity = $(this).children('identity').text();
			createActivityItem(identity, template, list);
		});
		template.remove();
		list.trigger('create');
	}
	activities.each(function() {
		var identity = $(this).children('identity').text();
		var value;
		if ('SUSPENDED' == $(this).children('execution').text()) {
			value = 'off';
		} else {
			value = 'on';
		}
		var select = list.find('select[name="' + ACTIVITY_FLIPSWITCH_PREFIX
				+ identity + '"]');
		if (loading) {
			select.val(value)
			if (authorized) {
				select.flipswitch("option", "disabled", true);
				select.flipswitch('enable');
			}
			select.flipswitch('refresh');
			$('#' + ACTIVITY_REQUEST_PREFIX + identity).on('vmouseup',
					requestChange);
		} else if (select.val() != value) {
			select.val(value).flipswitch('refresh');
		}
	});
	list.listview('refresh');
}

/*
 * Send a request to change the application/task status, and then update the
 * page to reflect this change.
 * 
 * param : args[0] If 'null' it does not change the state of the
 * application/task, otherwise it's suspended state will be set to that
 * specified in this argument.
 * 
 * param : args[1] If 'null' then the first argument will apply to the
 * application, otherwise it will be the name of the task to which the first
 * argument will apply.
 */
function updateActs(args) {
	var suspend = args[0];
	var activity = args[1];

	var data = null;
	var httpType = "GET";
	var mediaType = 'application/xml';
	var activityUrl;
	if (null == suspend || null == activity) {
		activityUrl = '../' + resolveDeployment() + '/report/'
				+ 'application/status';
	} else {
		var action;
		if (suspend) {
			action = 'suspend';
		} else {
			action = 'resume';
		}
		activityUrl = '../' + resolveDeployment() + '/command/activity/'
				+ activity + '/' + action;
		httpType = "POST";
	}
	$.ajax({
		url : activityUrl,
		success : function(data, textStatus, xmlHttpRequest) {
			updateActivityStates(data);
			refreshContents(updateActs, [ null, null ], pageTimer);
		},
		error : function(xmlHttpRequest, textStatus, errorThrown) {
			if (suspend && 'Not Found' == errorThrown) {
				alert('Not authorized');
				refreshContents(updateActs, [ null, null ], pageTimer);
				return;
			}
			if ('' == errorThrown) {
				alert(textStatus);
			} else {
				alert(errorThrown);
			}
			refreshContents(updateActs, [ null, null ], pageTimer);
		},
		contentType : mediaType,
		data : data,
		type : httpType
	});
}
