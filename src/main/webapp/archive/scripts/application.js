/*
 * This file contains the functions needed on the applcoation.html/index.html
 * page.
 */

/*
 * Updates the current status of the application.
 * 
 * param : data The data from which to extract the current state.
 */
function updateApplicationState(data) {
	var identity = $(data).find('identity:not(activity identity)').text();
	var select = $('select#application_flipswitch');
	var loading = $('#application_identity').text() != identity
	if (loading) {
		$('#application_identity').text(identity);
		// Enable on/off slider when setting the SPADE identity.
		if (authorized) {
			// Enable application suspend.resume
			select.flipswitch("option", "disabled", false);
			select.flipswitch('enable');

			// Allow commands to be displayed
			$('#spade_actions_panel').show();
		}
		select.flipswitch('option', 'theme', 'a');
		select.flipswitch('refresh');
		$('#application_request').on('vmouseup', requestChange);
	}

	var state = $(data).find('execution:not(activity execution)').text();
	var theme;
	var value;
	if ('RUNNING' == state) {
		theme = 'a';
		value = 'on';
	} else if ('PARTIALLY_RUNNING' == state) {
		theme = 'b';
		value = 'on';
	} else if ('PARTIALLY_SUSPENDED' == state) {
		theme = 'c';
		value = 'off';
	} else if ('SUSPENDED' == state) {
		theme = 'a';
		value = 'off';
	} else {
		// Should never get here!
		select.flipswitch('disable');
		return;
	}
	var existing = select.flipswitch("option", "theme");
	if (theme != existing) {
		setFlipswitchTheme(select, existing, theme);
	}
	if (select.val() != value) {
		select.val(value);
	}
	select.flipswitch('refresh')
}

/*
 * Send a request to change the application/task status, and then update the
 * page to reflect this change.
 * 
 * param : args[0] If 'null' it does not change the state of the
 * application/task, otherwise it's suspended state will be set to that
 * specified in this argument.
 * 
 * param : args[1] If 'null' then the first argument will apply to the
 * application, otherwise it will be the name of the task to which the first
 * argument will apply.
 */
function updateApp(arg) {
	var suspend = arg;

	var data = null;
	var httpType = "GET";
	var mediaType = 'application/xml';
	var activityUrl;
	if (null == suspend) {
		activityUrl = '../' + resolveDeployment() + '/report/'
				+ 'application/status';
	} else {
		var action;
		if (suspend) {
			action = 'suspend';
		} else {
			action = 'resume';
		}
		activityUrl = '../' + resolveDeployment() + '/command/'
				+ 'application/' + action;
		httpType = "POST";
	}
	$.ajax({
		url : activityUrl,
		success : function(data, textStatus, xmlHttpRequest) {
			updateApplicationState(data);
			refreshContents(updateApp, null, pageTimer);
		},
		error : function(xmlHttpRequest, textStatus, errorThrown) {
			if (suspend && 'Not Found' == errorThrown) {
				alert('Not authorized');
				refreshContents(updateApp, null, pageTimer);
				return;
			}
			if ('' == errorThrown) {
				alert(textStatus);
			} else {
				alert(errorThrown);
			}
			refreshContents(updateApp, null, pageTimer);
		},
		contentType : mediaType,
		data : data,
		type : httpType
	});
}

$('#spade_action_buttons :button').each(function() {
	$(this).button().click(executeCommand);
});

$(document).ready(function() {
	chart1 = createChart('application_counts_summary', 'application');
	chart1.series[0].hide();
	chart1.series[3].hide();
});

$(document).on('pageshow', '#activity_counts_page', function() {
	chart1 = createChart('selection_counts', 'activity');
	chart1.series[0].hide();
	chart1.series[3].hide();
});

$(document).on('pageshow', '#application_status_page', function() {
	updateApp(null);
});

$(document).on('pageshow', '#activity_status_page', function() {
	updateActs([ null, null ]);
});

$(document).on('pagebeforecreate', '#activity_status_page', function() {
	scriptLoader.load([ '../scripts/activities.js' ]);
});
