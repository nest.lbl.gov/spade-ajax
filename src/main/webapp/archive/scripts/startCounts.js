/*
 * This file contains the function needed to start the counts.js script.
 */

$(document).ready(function() {
	chart1 = createChart('selection_counts', parseQuery()['selection']);
	chart1.series[0].hide();
	chart1.series[3].hide();
});
