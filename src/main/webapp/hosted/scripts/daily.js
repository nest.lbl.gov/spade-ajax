var container_content_title = 'Spade ' + parse_query()['quantity'];
var awaiting_data_message = document.getElementById('awaiting_data_message').innerHTML;

/*
 * Creates an empty chart waiting for the data to be loaded.
 */
function create_empty_chart() {
    return Highcharts.stockChart('container', {
        credits: {
            enabled: true,
            href: "http://nest.lbl.gov/products/spade/",
            text: 'Spade Data Management'
        },
        title: {
            text: container_content_title
        },
        subtitle: {
            text: awaiting_data_message,
            useHTML: true
        },
        yAxis: [{
            labels: {
                align: 'left'
            },
            height: '80%',
            resize: {
                enabled: true
            }
        }, {
            labels: {
                align: 'left'
            },
            top: '80%',
            height: '20%',
            offset: 0
        }],
        series: [{
            name: 'size'
        }, {
            type: 'column',
            name: 'files',
            yAxis: 1
        }]
    });
};


	// If _not_ running behind a proxy map HTML domain to RESTful domain
var restfulDomain = self.location.href.replace("spade/hosted","spade/local");

Highcharts.getJSON((new URL('../report/flow/' + parse_query()[ 'quantity'], new URL(restfulDomain))).href, function (data) {

    var summary = data.splice(0,1);

    // split the data set into size and files
    var size = [],
        files = [],
        dataLength = data.length,
        i = 0;

    for (i; i < dataLength; i += 1) {
        size.push([
            data[i][0], // the date
            data[i][1]  // number of bytes
        ]);

        files.push([
            data[i][0], // the date
            data[i][2]  // number of files
        ]);
    }

    containers_content = Highcharts.stockChart('container', {
        credits: {
            enabled: true,
            href: "http://nest.lbl.gov/products/spade/",
            text: 'Spade Data Management'
        },
        tooltip: {
            formatter: function () {
                var s = '<b>' + Highcharts.dateFormat('%A, %b %e, %Y', this.x) + '</b>';
                this.points.forEach( function (point, i) {
                    if (point.series.name.indexOf('size', point.series.name.length - 4) !== -1) {
                        var decimal = 0;
                        var index = 0;
                        var value = point.y;
                        while (value > 1024 && index < BYTE_SUFFICES.length -1) {
                            decimal = 2;
                            index += 1;
                            value = value / 1024;
                        }
                        s += '<br/><span style="color:' + point.series.color + '">' + point.series.name + ': ' + value.toFixed(decimal) + ' ' + BYTE_SUFFICES[index];
                    } else {
                        s += '<br/><span style="color:' + point.series.color + '">' + point.series.name + ': ' + point.y;
                    }
                });
                return s;
            },
            shared: true
        },
        title: {
            text: container_content_title
        },
        subtitle: {
            text: awaiting_data_message,
            useHTML: true
        },
        rangeSelector: {
            selected: 0
        },
        yAxis: [{
            labels: {
                align: 'left'
            },
            height: '80%',
            resize: {
                enabled: true
            }
        }, {
            labels: {
                align: 'left'
            },
            top: '80%',
            height: '20%',
            offset: 0
        }],
        series: [{
            name: 'size',
            data: size
        }, {
            type: 'column',
            name: 'files',
            data: files,
            yAxis: 1
        }]
    });
	data_status = "LOADED";
    set_titles(summary[0][2], displayTimestamp(new Date(summary[0][1])));
});

containers_content = create_empty_chart();
var data_status = "WAITING";
var awaiting_data_interval = setInterval(awaiting_data, 1000);
