var container_content_title = 'Spade ' + parse_query()['quantity'];
var awaiting_data_message = document.getElementById('awaiting_data_message').innerHTML;

if (typeof Intl !== 'undefined') {
    // use Intl approach
    localTimezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
}
    
if (!localTimezone) {
    // use jstz approach in IE browser
    localTimezone = jstz.determine().name();
}

/*
 * Creates an empty chart waiting for the data to be loaded.
 */
function create_empty_chart(quantity, instance) {
	// If _not_ running behind a proxy map HTML domain to RESTful domain
	var restfulDomain = self.location.href.replace("spade/hosted","spade/local");
	var afterQuery;
	if(parse_query()[ 'after']) {
		afterQuery = "&after=" + parse_query()[ 'after'];
	} else {
		afterQuery = '';
	}

    return new Highcharts.Chart({
        chart: {
            renderTo: 'container',
            defaultSeriesType: 'column',
        },
        credits: {
            enabled: true,
            href: "http://nest.lbl.gov/products/spade/",
            text: 'Spade Data Management'
        },
        tooltip: {
            formatter: function () {
                var s = '<b>' + Highcharts.dateFormat('%A, %b %e, %Y', this.x) + '</b>';
                this.points.forEach( function (point, i) {
                    if (point.series.name.indexOf('size', point.series.name.length - 4) !== -1) {
                        var decimal = 0;
                        var index = 0;
                        var value = point.y;
                        while (value > 1024 && index < BYTE_SUFFICES.length -1) {
                            decimal = 2;
                            index += 1;
                            value = value / 1024;
                        }
                        s += '<br/><span style="color:' + point.series.color + '">' + point.series.name + ': ' + value.toFixed(decimal) + ' ' + BYTE_SUFFICES[index];
                    } else {
                        s += '<br/><span style="color:' + point.series.color + '">' + point.series.name + ': ' + point.y;
                    }
                });
                return s;
            },
            shared: true
        },
        time: {
            useUTC: false
        },
        title: {
            text: container_content_title
        },
        subtitle: {
            text: awaiting_data_message,
            useHTML: true
        },
        xAxis: {
            type: 'datetime'
        },
        yAxis:[ {
            title: {
                text: 'size'
            }
        }, {
            title: {
                text: 'files'
            },
            opposite: true
        }],
        series:[ {
            name: 'size',
        }, {
            name: 'files',
            yAxis: 1
        }],
        data: {
            columnsURL: (new URL('../report/flow/' + parse_query()[ 'quantity'] +
                                 '?fine=true&span=' + parse_query()[ 'span'] +
                                 afterQuery, new URL(restfulDomain))).href,
            enablePolling: true,
            dataRefreshRate: parse_query()[ 'interval'],
            complete: function (options) {

                data_status = "LOADED";
	        },
            parsed: function (data) {
                var summary = data.splice(0,1);
                set_titles(summary[0][2], displayTimestamp(new Date(summary[0][1])));
            }
        }
    });
};


containers_content = create_empty_chart(parse_query()[ 'quantity'],
                                        parse_query()[ 'instance']);
var data_status = "WAITING";
var awaiting_data_interval = setInterval(awaiting_data, 1000);
