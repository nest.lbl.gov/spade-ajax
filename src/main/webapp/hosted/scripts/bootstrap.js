/*
 * This file in included in all of the static web pages
 *   and provides a set of common functions most or all
 *   will use.
 */

var BYTE_SUFFICES =[ 'B', 'KB', 'MB', 'GB', 'TB', 'PB'];

/*
 * The following statements resize charts (and other content?) to
 *   fit inside their containing window.
 *
 * The following global variables need to be declared before invocation.
 *   containers_content : the container within the page with the chart
 *                        (and other content?)
 */
var containers_width = window.innerWidth - 16;
var containers_height = 400 - 16;

window.onresize = function () {
    containers_width = window.innerWidth - 16
    if (null != containers_content) {
        containers_content.setSize(containers_width, containers_height);
    }
};


// Associative array in which the URL's query parameters are cached.
var query_map = null;

/*
 * Function to fill query's associative array.
 */
function parse_query() {
    if (null != query_map) {
        return query_map;
    }
    var query_string = window.location.search.substring(1,
            window.location.search.length);
    var queries = query_string.split("&");
    query_map = new Array();
    for (index in queries) {
        var query_item = queries[index].split("=");
        query_map[query_item[0]] = unescape(query_item[1]);
    }
    if (null != query_defaults) {
        for ( var key in query_defaults) {
            if (null == query_map[key]) {
                query_map[key] = query_defaults[key];
            }
        }
    }
    return query_map;
}


/*
 *  Object that loads JavaScript files in sequence.
 */
var scriptLoader = {
    _loadScript : function(url, callback) {
        var head = document.getElementsByTagName('head')[0];
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = url;
        if (callback) {
            script.onreadystatechange = function() {
                if (this.readyState == 'loaded')
                    callback();
            };
            script.onload = callback;
        }
        head.appendChild(script);
    },

	/*
	 * Iteratively loads each item remaining the the "items" sequence.
	 */
    load : function(items, iteration) {
        if (!iteration)
            iteration = 0;
        if (items[iteration]) {
            scriptLoader._loadScript(items[iteration], function() {
                scriptLoader.load(items, iteration + 1);
            });
        }
    }
};


/*
 * Creates a displayable string from a JavaScript timestamp.
 * 
 * param : timestamp The JavaScript timestamp to convert.
 */
function displayTimestamp(timestamp) {
	var dayName = [ 'Sun', 'Mon', 'Tue', 'Wed', 'Thr', 'Fri', 'Sat' ];
	var monthName = [ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug',
			'Sep', 'Oct', 'Nov', 'Dec' ];
	var hours = timestamp.getHours();
	if (hours < 10) {
		hours = "0" + hours;
	}
	var minutes = timestamp.getMinutes();
	if (minutes < 10) {
		minutes = "0" + minutes;
	}
	return dayName[timestamp.getDay()] + ', ' + monthName[timestamp.getMonth()]
			+ ' ' + timestamp.getDate() + ' ' + timestamp.getFullYear() + ', '
			+ hours + ':' + minutes;
}


function set_titles(title, subtitle) {
    containers_content.setTitle({
        text: title
    }, {
        text: subtitle,
        useHTML: true
    });
}

/*
 * Function to update loading progress.
 * In the page that is using this function, there needs to be an element
 *   with the id="awaiting_data_message" with a &nbsp; for each dot that
 *   should appear while the data has not arrived (and initially contain
 *   no '.').
 *
 * The following global variables need to be declared before invocation.
 *   data_status : should be initialized a ""WAITING" and changed when
 *                 the data has appeared or failed to appear.
 *   awaiting_data_interval : an interval that will call this function.
 *   container_content_title : 
 *   awaiting_data_message :
 */

function awaiting_data() {
    if ("WAITING" != data_status) {
        awaiting_data_message = awaiting_data_message.replace(/\./g, '&nbsp;');;
        clearInterval(awaiting_data_interval);
        return;
    }
    dots = awaiting_data_message;
    if (-1 == dots.indexOf("&nbsp;")) {
        dots = dots.replace(/\./g, '&nbsp;');
    }
    dots = dots.replace(/&nbsp;/, '.');
    awaiting_data_message = dots
    set_titles(container_content_title, awaiting_data_message);
}
