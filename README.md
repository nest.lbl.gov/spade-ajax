[SPADE - AJAX](https://gitlab.com/nest.lbl.gov/spade-ajax) contains the interfaces that can be used to provide ajax access to [SPADE](https://gitlab.com/nest.lbl.gov/spade).

In order to build this project see the [developer notes](developer_notes.md).
