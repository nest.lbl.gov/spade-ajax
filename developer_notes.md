# Developer Notes for `spade-ajax` #

These notes are to help developers develop, maintain and debug the `spade-ajax` Java library.


# Index of Notes #

Below is a list of notes. They are in no particular order (as defining one and maintaining it does not seem the best use of time).

*   [Building `spade-ajax`](#building-spade-ajax)
*   [Deploying `spade-ajax`](#deploying-spade-ajax)


# Building `spade-ajax` #

The following assumes [maven](http://maven.apache.org/) is already installed and shows how to build and install a released version of `spade-ajax` locally.

    SPADE-AJAX_VERSION=2.1.1
    git clone git@gitlab.com:nest.lbl.gov/spade-ajax.git
    cd spade-ajax
    git checkout ${SPADE-AJAX_VERSION}
    mvn clean install

Clearly to build the `master` the `SPADE-AJAX_VERSION` should be set to that value.


# Deploying `spade-ajax` #

A locally installed `spade-ajax` application can be deployed to its maven respository using the following command.

    mvn deploy

For this to work transparently, the credentials, i.e. ssh key and config, that allow the user `nest` to log into `nest.lbl.gov` must be correctly configured, and the `~/.m2/settings.xml` file should contain an element like the following to set up the user name to be used to deploy to the `nest-maven2-projects` repository.


    <settings>
        ...
        <servers>
            ...
            <server>
                <id>nest-maven2-projects</id>
                <username>nest</username>
            </server>
            ...
        </servers>
        ...
    </settings>
